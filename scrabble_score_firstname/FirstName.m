//
//  FirstName.m
//  scrabble_score_firstname
//
//  Created by Rémy Bardou on 10/08/2016.
//  Copyright © 2016 rbardou. All rights reserved.
//

#import "FirstName.h"
#import "ScrabbleScore.h"

@implementation FirstName

SexType SexTypeFromString(NSString *string) {
	if ([string isEqualToString:@"F"]) return SexTypeFemale;
	else if ([string isEqualToString:@"M"]) return SexTypeMale;
	else if ([string isEqualToString:@"X"]) return SexTypeBoth;
	return SexTypeUndefined;
}

- (BOOL) isEqual:(FirstName *)object {
	if ([self.name isEqual:object.name]) return YES;
	return NO;
}

- (void) mergeWithFirstName:(FirstName *)otherFirstName {
	self.popularity += otherFirstName.popularity;
	
	if (otherFirstName.sexType != self.sexType) {
		self.sexType = SexTypeBoth;
	}
}

+ (instancetype) firstNameFromDictionary:(NSDictionary *)dictionary {
	if (![dictionary isKindOfClass:[NSDictionary class]]) return nil;
	
	NSDictionary *fields = [dictionary objectForKey:@"fields"];
	if (![dictionary isKindOfClass:[NSDictionary class]]) return nil;
	
	FirstName *fn = [FirstName new];
	fn.name = fields[@"prenoms"];
	
	NSString *myString = [[fn.name uppercaseString] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale localeWithLocaleIdentifier:@"fr_FR"]];
	fn.uniqueName = myString;
	
	fn.sexType = SexTypeFromString(fields[@"sexe"]);
	fn.popularity = [fields[@"nombre"] integerValue];
	
	return fn;
}

+ (NSArray<FirstName *> *) firstNamesFromDictionaries:(NSArray<NSDictionary *> *)dictionaries {
	NSMutableDictionary *results = [NSMutableDictionary new];
	for (NSDictionary *dic in dictionaries) {
		FirstName *fn = [self firstNameFromDictionary:dic];
		if (fn.uniqueName) {
			
			FirstName *existingFn = results[fn.uniqueName];
			if (existingFn) {
				[existingFn mergeWithFirstName:fn];
			} else {
				results[fn.uniqueName] = fn;
			}
		}
	}
	return [results allValues];
}

@end
