//
//  ViewController.m
//  scrabble_score_firstname
//
//  Created by Rémy Bardou on 10/08/2016.
//  Copyright © 2016 rbardou. All rights reserved.
//

#import "ViewController.h"
#import "FirstNameTableViewCell.h"
#import <MessageUI/MessageUI.h>

#import "FirstName.h"
#import "ScrabbleScore.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray<FirstName *> *allResults;
@property (nonatomic, strong) NSArray<FirstName *> *filteredResults;
@property (nonatomic, strong) NSMutableArray<FirstName *> *selectedFirstNames;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	
	[self.tableView registerNib:[FirstNameTableViewCell defaultNib] forCellReuseIdentifier:@"cell"];
	
	self.refreshControl = [[UIRefreshControl alloc] init];
	[self.tableView addSubview:self.refreshControl];
	[self.refreshControl addTarget:self action:@selector(fetchData) forControlEvents:UIControlEventValueChanged];
	
	if ([MFMailComposeViewController canSendMail]) {
		UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(didTapSelectionButton)];
		self.navigationItem.rightBarButtonItem = button;
	}

	[self fetchData];
}

- (void) fetchData {
	
	[self.refreshControl beginRefreshing];
	
	if (self.allResults.count == 0) {
		[self loadDataFromJsonWithCompletionHandler:^{
			[self filterDataWithCompletionHandler:^{
				[self reloadData];
			}];
		}];
	} else {
		[self filterDataWithCompletionHandler:^{
			[self reloadData];
		}];
	}
}

- (void) loadDataFromJsonWithCompletionHandler:(void(^)())completion {
	
	NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"firstnames_formatted" ofType:@"json"];
	NSData *jsonData = [[NSData alloc] initWithContentsOfFile:jsonPath];
	NSError *error = nil;
	NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		NSArray<FirstName *> *gResults = [FirstName firstNamesFromDictionaries:jsonArray];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			self.allResults = gResults;
			
			if (completion) completion();
		});
	});
}

- (void) filterDataWithCompletionHandler:(void(^)())completion {
	
	NSArray<FirstName *> *gAllResults = [self.allResults copy];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		
		NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(FirstName *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
			
			if (evaluatedObject.sexType == SexTypeMale) return NO;
			
			NSInteger firstLetterScore = [ScrabbleScore scoreForLetter:[evaluatedObject.uniqueName substringToIndex:1]];
			if (firstLetterScore != 1) return NO;
			
			NSInteger overallScore = [ScrabbleScore scoreForString:evaluatedObject.uniqueName];
			if (overallScore != 6) return NO;
			
			return YES;
		}];
		
		NSArray<FirstName *> *gResults = [[gAllResults filteredArrayUsingPredicate:filter] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"popularity" ascending:NO]]];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			self.filteredResults = gResults;
			
			if (completion) completion();
		});
	});
	
	
}

- (void) reloadData {
	[self.refreshControl endRefreshing];
	[self.tableView reloadData];
	
	self.navigationItem.title = [NSString stringWithFormat:@"%ld résultats sur %ld prénoms", (unsigned long)self.filteredResults.count, (unsigned long)self.allResults.count];
}

#pragma mark - TableView

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.filteredResults.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	FirstNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
	
	FirstName *fn = self.filteredResults[indexPath.row];
	cell.nameLabel.text = [fn.uniqueName capitalizedStringWithLocale:[NSLocale localeWithLocaleIdentifier:@"fr_FR"]];
	cell.countLabel.text = [NSString stringWithFormat:@"%ld", (long)fn.popularity];
	return cell;
}

- (void) didTapSelectionButton {
	
	void(^sendMail)(NSArray<FirstName *> *) = ^(NSArray<FirstName *> *firstNames) {
		MFMailComposeViewController *vc = [MFMailComposeViewController new];
		vc.mailComposeDelegate = self;
		[vc setSubject:@"J'ai trouvé !"];
		
		NSMutableString *list = [NSMutableString new];
		for (FirstName *fn in firstNames) {
			if (list.length > 0) {
				[list appendString:@"\n"];
			}
			[list appendString:[NSString stringWithFormat:@"- %@", fn.uniqueName.capitalizedString]];
		}
		
		[vc setMessageBody:[NSString stringWithFormat:@"C'est un de ces prénoms : \n%@", [list copy]] isHTML:NO];
		[self.navigationController presentViewController:vc animated:YES completion:nil];
	};
	
	void(^sendSMS)(NSArray<FirstName *> *) = ^(NSArray<FirstName *> *firstNames) {
		MFMessageComposeViewController *vc = [MFMessageComposeViewController new];
		vc.messageComposeDelegate = self;
		
		NSMutableString *list = [NSMutableString new];
		for (FirstName *fn in firstNames) {
			if (list.length > 0) {
				[list appendString:@"\n"];
			}
			[list appendString:[NSString stringWithFormat:@"- %@", fn.uniqueName.capitalizedString]];
		}
		
		[vc setBody:[NSString stringWithFormat:@"C'est un de ces prénoms : \n%@", [list copy]]];
		[self.navigationController presentViewController:vc animated:YES completion:nil];
	};
	
	void(^shareList)(NSArray<FirstName *> *) = ^(NSArray<FirstName *> *firstNames) {
		
		UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Méthode de partage" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
		
		if ([MFMailComposeViewController canSendMail]) {
			[controller addAction:[UIAlertAction actionWithTitle:@"Mail" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
				sendMail(firstNames);
			}]];
		}
		
		if ([MFMessageComposeViewController canSendText]) {
			[controller addAction:[UIAlertAction actionWithTitle:@"SMS" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
				sendSMS(firstNames);
			}]];
		}
		
		if (controller.actions.count == 1) {
			
			if ([MFMailComposeViewController canSendMail]) {
				sendMail(firstNames);
			}
			else if ([MFMessageComposeViewController canSendText]) {
				sendSMS(firstNames);
			}
			
		} else {
			[self.navigationController presentViewController:controller animated:YES completion:nil];
		}
	};

	
	UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Partage" message:@"Que voulez-vous partager ?" preferredStyle:UIAlertControllerStyleActionSheet];
	
	if (self.tableView.indexPathsForSelectedRows.count > 0) {
		[controller addAction:[UIAlertAction actionWithTitle:@"Les prénoms sélectionnés" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
			
			NSMutableArray *selection = [NSMutableArray new];
			for (NSIndexPath *indexPath in self.tableView.indexPathsForSelectedRows) {
				FirstName *fn = self.filteredResults[indexPath.row];
				[selection addObject:fn];
			}
			
			shareList([selection copy]);
		}]];
	}
	
	[controller addAction:[UIAlertAction actionWithTitle:@"Tous les résultats" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		shareList(self.filteredResults);
	}]];
	
	[controller addAction:[UIAlertAction actionWithTitle:@"Annuler" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
		
	}]];
	
	[self.navigationController presentViewController:controller animated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
	[controller dismissViewControllerAnimated:YES completion:nil];
}

- (void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
	[controller dismissViewControllerAnimated:YES completion:nil];
}

@end
