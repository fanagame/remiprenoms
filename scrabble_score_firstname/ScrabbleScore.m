//
//  ScrabbleScore.m
//  scrabble_score_firstname
//
//  Created by Rémy Bardou on 10/08/2016.
//  Copyright © 2016 rbardou. All rights reserved.
//

#import "ScrabbleScore.h"

@implementation ScrabbleScore

+ (NSInteger) scoreForString:(NSString *)myString {
	NSInteger score = 0;
	
	NSUInteger length = myString.length;
	for (int i = 0; i < length; i++) {
		NSString *letter = [myString substringWithRange:NSMakeRange(i, 1)];
		score += [self scoreForLetter:[letter uppercaseString]];
	}
	
//	NSLog(@"%@ => %ld", myString, score);
	return score;
}

+ (NSInteger) scoreForLetter:(NSString *)letter {
	if (letter.length != 1) return 0;
	
	if ([letter isEqualToString:@"E"] ||
		[letter isEqualToString:@"A"] ||
		[letter isEqualToString:@"I"] ||
		[letter isEqualToString:@"N"] ||
		[letter isEqualToString:@"O"] ||
		[letter isEqualToString:@"R"] ||
		[letter isEqualToString:@"S"] ||
		[letter isEqualToString:@"T"] ||
		[letter isEqualToString:@"U"] ||
		[letter isEqualToString:@"L"]) {
		return 1;
	}
	else if ([letter isEqualToString:@"D"] ||
			 [letter isEqualToString:@"M"] ||
			 [letter isEqualToString:@"G"]) {
		return 2;
	}
	else if ([letter isEqualToString:@"B"] ||
			 [letter isEqualToString:@"C"] ||
			 [letter isEqualToString:@"P"]) {
		return 3;
	}
	else if ([letter isEqualToString:@"F"] ||
			 [letter isEqualToString:@"H"] ||
			 [letter isEqualToString:@"V"]) {
		return 4;
	}
	else if ([letter isEqualToString:@"J"] ||
			 [letter isEqualToString:@"Q"]) {
		return 8;
	}
	else if ([letter isEqualToString:@"K"] ||
			 [letter isEqualToString:@"W"] ||
			 [letter isEqualToString:@"X"] ||
			 [letter isEqualToString:@"Y"] ||
			 [letter isEqualToString:@"Z"]) {
		return 10;
	}
	
	return 0;
}

@end
