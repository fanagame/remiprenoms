//
//  ScrabbleScore.h
//  scrabble_score_firstname
//
//  Created by Rémy Bardou on 10/08/2016.
//  Copyright © 2016 rbardou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScrabbleScore : NSObject

+ (NSInteger) scoreForString:(NSString *)string;
+ (NSInteger) scoreForLetter:(NSString *)letter;

@end
