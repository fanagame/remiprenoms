//
//  FirstNameTableViewCell.m
//  scrabble_score_firstname
//
//  Created by Rémy Bardou on 10/08/2016.
//  Copyright © 2016 rbardou. All rights reserved.
//

#import "FirstNameTableViewCell.h"

@implementation FirstNameTableViewCell

+ (UINib *)defaultNib {
	NSString *nibName = NSStringFromClass(self.class);
	return [UINib nibWithNibName:nibName bundle:[NSBundle mainBundle]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
