//
//  FirstNameTableViewCell.h
//  scrabble_score_firstname
//
//  Created by Rémy Bardou on 10/08/2016.
//  Copyright © 2016 rbardou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstNameTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;

+ (UINib *)defaultNib;

@end
