//
//  FirstName.h
//  scrabble_score_firstname
//
//  Created by Rémy Bardou on 10/08/2016.
//  Copyright © 2016 rbardou. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(uint8_t, SexType) {
	SexTypeUndefined,
	SexTypeMale,
	SexTypeFemale,
	SexTypeBoth
};

@interface FirstName : NSObject

@property (nonatomic, copy) NSString *uniqueName;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) SexType sexType;
@property (nonatomic, assign) NSInteger popularity;

+ (instancetype) firstNameFromDictionary:(NSDictionary *)dictionary;
+ (NSArray<FirstName *> *) firstNamesFromDictionaries:(NSArray<NSDictionary *> *)dictionaries;

@end
